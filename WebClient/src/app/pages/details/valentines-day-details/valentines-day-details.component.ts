import { Component, OnInit } from '@angular/core';
import { ValentinesdayService } from 'src/app/dummyServices/catalogs/valentinesday.service';
import { ImageInterface } from 'src/app/models/image-interface';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-valentines-day-details',
  templateUrl: './valentines-day-details.component.html',
  styleUrls: ['./valentines-day-details.component.css'],
  providers: [NgbModal]
})
export class ValentinesDayDetailsComponent implements OnInit {

  image: ImageInterface;

  constructor(
    private valentinesDayService: ValentinesdayService,
    private route: ActivatedRoute,
    private router: Router,
    private modalService: NgbModal) { }


  ngOnInit() {
    this.route.params.subscribe(param => {
      if (param && param.id) {
        this.image = this.valentinesDayService.getImage(param.id);
      }
    });
  }

  goBack() {
    window.history.back();
  }

  open(content) {
    this.modalService.open(content,  { centered: true });
  }

}
