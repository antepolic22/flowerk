import { BirthdaysService } from './../../../dummyServices/catalogs/birthdays.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ImageInterface } from 'src/app/models/image-interface';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-birthday-details',
  templateUrl: './birthday-details.component.html',
  styleUrls: ['./birthday-details.component.css']
})
export class BirthdayDetailsComponent implements OnInit {

  image: ImageInterface;

  constructor(
    private birthdayService: BirthdaysService,
    private route: ActivatedRoute,
    private router: Router,
    private modalService: NgbModal) { }


  ngOnInit() {
    this.route.params.subscribe(param => {
      if (param && param.id) {
        this.image = this.birthdayService.getImage(param.id);
      }
    });
  }

  goBack() {
    window.history.back();
  }

  open(content) {
    this.modalService.open(content,  { centered: true });
  }
}
