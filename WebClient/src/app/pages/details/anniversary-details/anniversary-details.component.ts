import { AnniversaryService } from './../../../dummyServices/catalogs/anniversary.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ImageInterface } from 'src/app/models/image-interface';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-anniversary-details',
  templateUrl: './anniversary-details.component.html',
  styleUrls: ['./anniversary-details.component.css']
})
export class AnniversaryDetailsComponent implements OnInit {

  image: ImageInterface;

  constructor(
    private anniversaryDayService: AnniversaryService,
    private route: ActivatedRoute,
    private router: Router,
    private modalService: NgbModal) { }


  ngOnInit() {
    this.route.params.subscribe(param => {
      if (param && param.id) {
        this.image = this.anniversaryDayService.getImage(param.id);
      }
    });
  }

  goBack() {
    window.history.back();
  }

  open(content) {
    this.modalService.open(content,  { centered: true });
  }

}
