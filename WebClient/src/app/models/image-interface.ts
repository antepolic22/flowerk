export class ImageInterface {
    id: number;
    src: string;
    title: string;
    description: string;
    category: string;
    priceFrom: string;
    priceTo: string;
}
