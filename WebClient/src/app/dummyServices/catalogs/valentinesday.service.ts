import { Injectable } from '@angular/core';
import { ImageInterface } from 'src/app/models/image-interface';

@Injectable({
  providedIn: 'root'
})
export class ValentinesdayService {

  images: ImageInterface[] = [
    {
      id: 1,
      src: 'assets/img/image1.jpg',
      title: 'Valentine 1',
      // tslint:disable-next-line:max-line-length
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc quam urna.Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
      category: 'Valentine\'s day',
      priceFrom: '100,00',
      priceTo: '500,00',
    },
    {
      id: 2,
      src: 'assets/img/image2.jpg',
      title: 'Valentine 2',
      // tslint:disable-next-line:max-line-length
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc quam urna.Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
      category: 'Valentine\'s day',
      priceFrom: '100,00',
      priceTo: '500,00',
    },
    {
      id: 3,
      src: 'assets/img/image3.jpg',
      title: 'Valentine 3',
      // tslint:disable-next-line:max-line-length
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc quam urna.Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
      category: 'Valentine\'s day',
      priceFrom: '100,00',
      priceTo: '500,00',
    },
  ];


  constructor() { }

  getImages() {
    return this.images;
  }

  getImage(id: number): ImageInterface {
    let img: ImageInterface;
    this.images.map(val => {
      if (val.id === Number(id)) {
         img = val;
      }
    });
    return img;
  }
}
