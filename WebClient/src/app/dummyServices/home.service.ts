import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HomeService {

  homeImages = [
    {
      img: 'assets/images/rose1.jpg',
    },
     {
      img: 'assets/images/rose2.jpg'
    },
    {
      img: 'assets/images/rose3.jpg'
    },
  ];


  constructor() { }

  getHomeImages() {
    return this.homeImages;
  }
}
